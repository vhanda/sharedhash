#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>

#include <KApplication>
#include <KDebug>

#include "sharedhash.h"

class TestObject : public QObject {
    Q_OBJECT

public slots:
    void main();
public:
    TestObject()
        : hash("uniqueidentifier3")
    {
        QTimer::singleShot( 0, this, SLOT(main()) );
    }

    SharedHash<int, int> hash;
};

int main( int argc, char ** argv ) {
    KComponentData component( QByteArray("nepomuk-test") );
    QCoreApplication app( argc, argv );

    TestObject a;
    app.exec();
}


void TestObject::main()
{
    kDebug() << 1 << hash.value(1);
    kDebug() << 2 << hash.value(2);

    hash.insert(1, 100);
    hash.insert(2, 200);

    kDebug() << 1 << hash.value(1);
    kDebug() << 2 << hash.value(2);

    hash.insert(0, 0);
    hash.insert(3, 300);
    hash.insert(4, 400);

    kDebug() << "4:" << hash.value(4);
    kDebug() << "Removing";
    hash.remove(4);
    kDebug() << "4:" << hash.value(4);

    kDebug() << "3:" << hash.value(3);
    kDebug() << "Removing";
    hash.remove(3);
    kDebug() << "3:" << hash.value(3);

    hash.insert(5, 500);
    hash.insert(6, 600);
    kDebug() << "6: " << hash.value(6);
    hash.insert(7, 700);
    hash.insert(8, 800);

    kDebug() << "FIRST";
    kDebug() << "Output contents";
    SharedHash<int, int>::iterator it = hash.begin();
    for(; it != hash.end(); it++ ) {
        kDebug() << it.key() << " -> " << it.value();
    }

    hash.insert(9, 900);

    kDebug() << "FIRST";
    kDebug() << "Output contents";
    it = hash.begin();
    for(; it != hash.end(); it++ ) {
        kDebug() << it.key() << " -> " << it.value();
    }

    hash.insert(71, 700);
    hash.insert(81, 800);
    hash.insert(91, 900);

    kDebug() << "Output contents";
    it = hash.begin();
    for(; it != hash.end(); it++ ) {
        kDebug() << it.key() << " -> " << it.value();
    }

    hash.insert(711, 700);
    hash.insert(811, 800);
    hash.insert(911, 900);

    kDebug() << "Output contents";
    it = hash.begin();
    for(; it != hash.end(); it++ ) {
        kDebug() << it.key() << " -> " << it.value();
    }

    hash.insert(111, 700);
    hash.insert(211, 800);
    hash.insert(311, 900);

    hash.insert(1, 666);
    kDebug() << 1 << hash.value(1);

    kDebug() << "Output contents";
    it = hash.begin();
    for(; it != hash.end(); it++ ) {
        kDebug() << it.key() << " -> " << it.value();
    }

    kDebug() << "----" << hash.find(8).value();
    kDebug() << hash.toQHash();
    kDebug() << "Exiting";
    QApplication::instance()->exit(0);

//    kDebug() << "Vishesh" << hash.value("Vishesh");
//    kDebug() << "Handa" << hash.value("Handa");
//
 //   hash.insert( "Vishesh", 1 );
  //  hash.insert( "Handa", 2 );

   // kDebug() << "Vishesh" << hash.value("Vishesh");
    //kDebug() << "Handa" << hash.value("Handa");
}

#include "main.moc"
