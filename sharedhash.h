/*
    Copyright (c) 2012, Vishesh Handa <handa.vish@gmail.com>
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
        * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
        * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY Vishesh Handa <handa.vish@gmail.com> ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL Vishesh Handa <handa.vish@gmail.com> BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef SHAREDHASH_H
#define SHAREDHASH_H

#include <QtCore/QSharedMemory>
#include <QtCore/QHash>
#include <QUuid>
#include <KDebug>

template<class Key, class Value>
class SharedHash
{
public:
    SharedHash(const QString& identifier);
    ~SharedHash();

    void insert(const Key& key, const Value& value);
    Value value(const Key& key);
    void remove(const Key& key);

    QString identifier() const { return m_sharedName.key(); }

    class iterator {
        int o;
        SharedHash<Key, Value>* hash;

        friend class SharedHash<Key, Value>;
    public:
        typedef std::bidirectional_iterator_tag iterator_category;
        typedef Value value_type;
        typedef Value* pointer;
        typedef Value& reference;

        //WARNING: Not safe -  The reason is that after we get the bucket the sharedData is no
        // longer locked, and some other process could be modifying the same memory.
        inline const Key& key() const {
            return hash->bucket(o)->key;
        }

        inline Value& value() const {
            return hash->bucket(o)->value;
        }

        inline Value& operator*() const {
            return value();
        }

        inline Value* operator->() const {
            return value();
        }

        inline bool operator==(const iterator& it) const {
            return o == it.o && hash == it.hash;
        }

        inline bool operator!=(const iterator& it) const {
            return o != it.o || hash != it.hash;
        }

        // Prefix
        inline iterator& operator++() {
            o = hash->nextBucketOffset(o);
            return *this;
        }

        // Postfix
        inline iterator& operator++(int) {
            o = hash->nextBucketOffset(o);
            return *this;
        }

        // Prefix
        inline iterator& operator--() {
            o = hash->prevBucketOffset(o);
            return *this;
        }

        // Postfix
        inline iterator& operator--(int) {
            o = hash->prevBucketOffset(o);
            return *this;
        }

        inline iterator& operator+(int j) {
            iterator it = *this;
            if( j > 0 )
                while( j-- )
                    it++;
            else if( j < 0 )
                while( j++ )
                    it--;
            return it;
        }

        inline iterator& operator-(int j) {
            return operator+(-j);
        }

        inline iterator& operator+=(int j) {
            return *this = *this + j;
        }

        inline iterator& operator-=(int j) {
            return *this = *this + (-j);
        }
    };
    friend class iterator;

    // A const_iterator is the same as an iterator as the shared memory could be changed by some
    // other application at any point of time
    typedef iterator const_iterator;

    // STL style iterators
    inline iterator begin() {
        iterator it;
        it.o = 0;
        it.hash = this;

        Bucket* b = bucket(it.o);
        if( !b || b->invalid() )
            it++;

        return it;
    }

    inline iterator end() {
        iterator it;
        it.o = -1;
        it.hash = this;

        return it;
    }

    int size() {
        updateLocalVariables();
        return m_meta->size;
    }

    int isEmpty() {
        return size() == 0;
    }
    int empty() {
        return isEmpty();
    }

    void clear();

    iterator find(const Key& key);

    QHash<Key, Value> toQHash();
private:
    struct Bucket {
        Key key;
        Value value;
        uint hash;

        // Offset to the next bucket (from the start)
        int link;

        bool invalid() const { return link == -2; }
        bool valid() const { return link != -2; }
        void setInvalid() { link = -2; }
    };

    const static int HashKeySize = 64 - sizeof(int);
    struct HashName {
        int id;
        char key[HashKeySize];
    };
    QSharedMemory m_sharedName;
    int m_keyId;

    QSharedMemory* m_sharedData;

    struct HashMetaData {
        int size;
        int capacity;
        int invalid;
        int emptyBucket;
    };
    HashMetaData* m_meta;

    int* m_buckets;
    Bucket* m_bucketData;

    void updateLocalVariables();
    void rehash(int newCapacity);

    QSharedMemory* createMemoryForCapacity( int capacity ) {
        int memorySize = sizeof(int)*4 + sizeof(int)*capacity + sizeof(Bucket)*capacity;
        QSharedMemory* memory = new QSharedMemory();

        while(1) {
            const static QString prefix = QString::fromLatin1("sharedhash:/");
            QString keyName =  prefix + QUuid::createUuid().toString().mid(1, HashKeySize-prefix.length());
            memory->setKey( keyName );
            if( memory->create( memorySize ) )
                return memory;

            if( memory->error() == QSharedMemory::AlreadyExists )
                continue;
            else if( memory->error() == QSharedMemory::InvalidSize ) {
                delete memory;
                return 0;
            }
        }
    }

    //WARNING: When the caller accesses the value of b, won't it need to lock m_sharedData?
    inline Bucket* bucket(int offset) {
        updateLocalVariables();

        m_sharedData->lock();
        if( offset < 0 || offset >= m_meta->size + m_meta->invalid ) {
            m_sharedData->unlock();
            return 0;
        }
        Bucket* b = &m_bucketData[offset];
        m_sharedData->unlock();

        return b;
    }

    inline int nextBucketOffset(int i) {
        Bucket* b = bucket(i);
        if( !b )
            return -1;

        m_sharedData->lock();
        int size = m_meta->size + m_meta->invalid;
        while( 1 ) {
            b++;

            // Over bound
            if( b >= (m_bucketData + size) ) {
                m_sharedData->unlock();
                return -1;
            }
            if( b->valid() )
                break;
        }

        int off = b - m_bucketData;
        m_sharedData->unlock();
        return off;
    }

    inline int prevBucketOffset(int i) {
        Bucket* b = bucket(i);
        if( !b )
            return -1;

        m_sharedData->lock();
        while( 1 ) {
            b--;

            // Under bound
            if( b < m_bucketData ) {
                m_sharedData->unlock();
                return -1;
            }
            if( b->valid() )
                break;
        }

        int off = b - m_bucketData;
        m_sharedData->unlock();
        return off;
    }
};

//
// The data is organized as follows -
// -------------------------------------------
// | key number | shared memory key as char* |
// -------------------------------------------
//
// key number = An int32 specific to the shared memory key
// shared memory key = Key of the shared memory where the actual data is lying
//
// They key number is just an optimization trick so that we avoid comparing the char* key
// before each operation. Integer comparison is faster than char* comparison.
//
// The actual shared memory is organized as follows -
// -------------------------------------------------------------------------
// | size | capacity | invalid | empty-bucket | int[capacity] | Bucket[capacity] |
// -------------------------------------------------------------------------
//
// size = capacity => int32
// invalid => number of invalid entires
// empty-bucket => integer offset for next emtpy bucket from Bucket[capacity]
// int[capacity] => Holds integer offsets to the bukcets. We use offsets cause the shared hash
//                  handles collisions via chaining
// Bucket[capacity] => Holds the actual buckets. In normal hashes these are individually
//                     allocated via new, but we cannot do that as we are using shared memory
//
//
// We are using 2 shared memory regions per hash cause we may need to resize our shared memory.
// Resizing the shared memory is not possible while maintaining the same identifier.
// Thus we use 2 shared memory regions: -
//      One to store the second memory's identifier
//      Second to store the actual hash map internals
//
template<class Key, class Value>
SharedHash<Key, Value>::SharedHash(const QString& identifier)
    : m_keyId(0)
    , m_sharedData(0)
{
    m_sharedName.setKey(identifier);
    if(m_sharedName.attach()) {
        updateLocalVariables();
    }
    else {
        if(m_sharedName.error() == QSharedMemory::NotFound) {
            if(!m_sharedName.create(sizeof(HashName))) {
                kError() << "Error creating the sharedName .. now what?";
                kError() << m_sharedName.errorString();
                return;
            }

            int capacity = 8;
            m_sharedData = createMemoryForCapacity( capacity );
            if( !m_sharedData )
                return;

            //
            // Initialize the SharedName memory
            //
            {
                m_sharedName.lock();
                HashName* hashName = static_cast<HashName*>(m_sharedName.data());
                hashName->id = 1;

                const QByteArray bytes = m_sharedData->key().toAscii();
                memcpy( hashName->key, bytes.data(), bytes.size() );
                m_sharedName.unlock();

                m_keyId = 1;
            }

            //
            // Initialize the Shared Data
            //
            m_sharedData->lock();

            m_meta = static_cast<HashMetaData*>(m_sharedData->data());
            m_meta->size = 0;
            m_meta->capacity = capacity;
            m_meta->invalid = 0;
            m_meta->emptyBucket = 0;

            char* data = static_cast<char*>(m_sharedData->data());
            data += sizeof(HashMetaData)/sizeof(char);

            m_buckets = reinterpret_cast<int*>(data);
            data += capacity * sizeof(int)/sizeof(char);
            memset( m_buckets, -1, capacity * sizeof(int) );

            m_bucketData = reinterpret_cast<Bucket*>(data);

            m_sharedData->unlock();
        }
        //FIXME: Check for other errors
    }
}

template<class Key, class Value>
SharedHash<Key, Value>::~SharedHash()
{
    delete m_sharedData;
}

template<class Key, class Value>
typename SharedHash<Key, Value>::iterator SharedHash<Key, Value>::find(const Key& key)
{
    updateLocalVariables();

    uint hash = qHash(key);
    uint index = hash % m_meta->capacity;

    int offset = m_buckets[index];
    while(offset != -1) {
        Bucket* bucket = &m_bucketData[offset];

        if( bucket->key == key ) {
            iterator it;
            it.o = offset;
            it.hash = this;
            return it;
        }
        offset = bucket->link;
    }

    return end();
}

template<class Key, class Value>
Value SharedHash<Key, Value>::value(const Key& key)
{
    updateLocalVariables();

    uint hash = qHash(key);
    uint index = hash % m_meta->capacity;

    int offset = m_buckets[index];

    while(offset != -1) {
        Bucket* bucket = &m_bucketData[offset];

        if( bucket->key == key ) {
            return bucket->value;
        }
        offset = bucket->link;
    }

    return Value();
}

template<class Key, class Value>
void SharedHash<Key, Value>::insert(const Key& key, const Value& value)
{
    updateLocalVariables();

    uint hash = qHash(key);
    uint index = hash % m_meta->capacity;

    m_sharedData->lock();
    int existingOffset = m_buckets[index];
    if( existingOffset != -1 ) {
        int offset = existingOffset;
        while( offset != -1 ) {
            Bucket* existingBucket = &m_bucketData[offset];
            if( existingBucket->key == key ) {
                existingBucket->value = value;
                m_sharedData->unlock();
                return;
            }
            offset = existingBucket->link;
        }
    }

    //FIXME: What if the exact key value pair already exist?
    m_buckets[index] = m_meta->emptyBucket;

    Bucket *newBucket = &m_bucketData[m_meta->emptyBucket];
    newBucket->hash = hash;
    newBucket->key = key;
    newBucket->value = value;
    newBucket->link = existingOffset;

    m_meta->emptyBucket++;
    m_meta->size++;

    m_sharedData->unlock();

    float loadRatio = (m_meta->size + m_meta->invalid) / m_meta->capacity;
    if( loadRatio > 0.80 )
        rehash( m_meta->capacity * 2 );
}


template<class Key, class Value>
void SharedHash<Key, Value>::rehash(int newCapacity)
{
    m_sharedName.lock();

    // Allocate the new data
    QSharedMemory* newData = createMemoryForCapacity( newCapacity );
    QByteArray bytes = newData->key().toAscii();

    HashName* hashName = static_cast<HashName*>(m_sharedName.data());
    hashName->id++;

    memcpy(hashName->key, bytes.data(), bytes.size());

    m_sharedName.unlock();

    m_sharedData->lock();
    newData->lock();

    //
    // Hash metadata
    HashMetaData* newMeta = static_cast<HashMetaData*>(newData->data());
    newMeta->size = m_meta->size;
    newMeta->capacity = newCapacity;
    newMeta->invalid = 0;
    newMeta->emptyBucket = 0;

    int oldTotalSize = m_meta->size + m_meta->invalid;
    m_meta = newMeta;

    //
    // Hash buckets
    char* data = static_cast<char*>(newData->data());
    data += sizeof(HashMetaData)/sizeof(char);

    m_buckets = reinterpret_cast<int*>(data);
    data += newCapacity * sizeof(int)/sizeof(char);

    memset( m_buckets, -1, newCapacity * sizeof(int) );

    //
    // Hash bucket data
    Bucket* oldBucketData = m_bucketData;
    m_bucketData = reinterpret_cast<Bucket*>(data);

    QSharedMemory* oldData = m_sharedData;
    m_sharedData = newData;

    // foreach of the old bucket, insert it in this one with the new capacity
    for(int i=0; i<oldTotalSize; i++) {
        // Copy the bucket
        Bucket* oldBucket = &oldBucketData[i];
        if( !oldBucket->valid() )
            continue;

        Bucket* newBucket = &m_bucketData[m_meta->emptyBucket];
        *newBucket = *oldBucket;
        newBucket->link = -1;

        // Reinsert it
        int index = newBucket->hash % newCapacity;
        int existingOffset = m_buckets[index];
        if( existingOffset != -1 ) {
            newBucket->link = existingOffset;
        }
        m_buckets[index] = m_meta->emptyBucket;
        m_meta->emptyBucket++;
    }

    m_sharedData->unlock();
    oldData->unlock();

    // Set this new uuid in the m_sharedName
    // increment the value of changed
    m_keyId++;

    // Delete the old shared memory
    delete oldData;
}

template<class Key, class Value>
void SharedHash<Key, Value>::updateLocalVariables()
{
    m_sharedName.lock();
    HashName* hashName = static_cast<HashName*>(m_sharedName.data());

    // Check if our value is different that theirs
    //          -> if not return
    if(hashName->id == m_keyId) {
        m_sharedName.unlock();
        return;
    }

    // Fetch the uuid
    QString keyString = QString::fromAscii(hashName->key);

    // Attach a sharedMemory to the uuid
    delete m_sharedData;
    m_sharedData = new QSharedMemory(keyString);
    if(!m_sharedData->attach()) {
        kError() << "Could not attach to the key WTF!! - " << keyString;
        kError() << m_sharedData->errorString();
        m_sharedName.unlock();
        return;
    }

    m_sharedData->lock();
    m_meta = static_cast<HashMetaData*>(m_sharedData->data());

    char* data = static_cast<char*>(m_sharedData->data());
    data += sizeof(HashMetaData)/sizeof(char);

    // Set the bucket*
    m_buckets = reinterpret_cast<int*>( data );
    data += m_meta->capacity * sizeof(int)/sizeof(char);

    m_bucketData = reinterpret_cast<Bucket*>( data );

    // Set the actual buckets
    m_sharedData->unlock();

    // Update our value to the new value
    m_keyId = hashName->id;

    m_sharedName.unlock();
}

template<class Key, class Value>
void SharedHash<Key, Value>::remove(const Key& key)
{
    // One option is to remove that data entry, and mark it as invalid
    // then when one is rehashing, copy only the ones that have changed
    // also updates all the offsets
    updateLocalVariables();

    uint hash = qHash(key);
    uint index = hash % m_meta->capacity;

    m_sharedData->lock();
    int offset = m_buckets[index];
    // Does not exist, just return
    if( offset == -1 ) {
        m_sharedData->unlock();
        return;
    }

    Bucket* bucket = &m_bucketData[offset];
    // only one element in the chain
    if( bucket->link == -1 ) {
        m_buckets[index] = -1;
    }
    else {
        m_buckets[index] = bucket->link;
    }

    // The Bucket position is just ignored. Eventually it will be reused when doing a rehash
    bucket->setInvalid();

    m_meta->size--;
    m_meta->invalid++;

    m_sharedData->unlock();
}

template<class Key, class Value>
QHash<Key, Value> SharedHash<Key, Value>::toQHash()
{
    QHash<Key, Value> hash;

    updateLocalVariables();
    m_sharedData->lock();

    int size = m_meta->size + m_meta->invalid;
    for(int i=0; i<size; i++) {
        Bucket* b = &m_bucketData[i];
        hash.insert( b->key, b->value );
    }

    m_sharedData->unlock();
    return hash;
}

template<class Key, class Value>
void SharedHash<Key, Value>::clear()
{
    updateLocalVariables();

    m_sharedData->lock();

    // The capacity remains the same
    m_meta->size = 0;
    m_meta->invalid = 0;
    m_meta->emptyBucket = 0;

    memset( m_buckets, -1, sizeof(int) * m_meta->capacity );
    memset( m_bucketData, 0, sizeof(Bucket) * m_meta->capacity );

    m_sharedData->unlock();
}



#endif // SHAREDHASH_H
